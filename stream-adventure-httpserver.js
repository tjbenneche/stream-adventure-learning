var http = require('http')
var fs = require('fs')
var through = require('through2')
var concat = require('concat-stream')

var tr = through(function(buf, _, next) {
  var up = buf.toString().toUpperCase()
  this.push(up)
  next()
})


var server = http.createServer(function(req, res) {
  if (req.method === 'POST') {
    req.pipe(tr).pipe(res)
  }
})

server.listen(process.argv[2])
