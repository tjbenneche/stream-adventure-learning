var through = require('through2')
var split = require('split')

var i = 0
var tr = through(function(buf, _, next) {
  var up = i % 2 ? buf.toString().toUpperCase() : buf.toString().toLowerCase()
  up += '\n'
  this.push(up)
  i++
  next()
})

process.stdin
  .pipe(split())
  .pipe(tr)
  .pipe(process.stdout)
